import "./App.css";
import { Route, Switch } from "react-router-dom";
import Navbar from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import Home from "./pages/Home/Home";
import Indications from "./pages/Indications/Indications";
import Login from "./pages/Login/Login";
import MyProducts from "./pages/MyProducts/MyProducts";
import Producer from "./pages/Producer/Producer";
import Product from "./pages/Product/Product";
import Products from "./pages/Products/Products";
import Register from "./pages/Register/Register";
import ProductDetails from "./pages/ProductDetails/ProductDetails";
import ShopCart from "./pages/ShopCart/ShopCart";
import ErrorComponent from "./components/ErrorComponent/ErrorComponent";
import ScrollToTop from "./ScrollToTop";
import PrivateRoute from "./components/PrivateRoute/PrivateRoute";
import Button from "./components/Button/Button";

function App() {
  return (
    <div className="App">
      <Navbar />
      <ScrollToTop />
      <Switch>
        <Route exact path="/">
          <Home />
        </Route>
        <Route exact path="/indications">
          <Indications />
        </Route>
        <Route exact path="/products">
          <Products />
        </Route>
        <Route exact path="/">
          <Home />
        </Route>
        {/* <Route exact path="/my-products">
          <MyProducts />
        </Route> */}
        <PrivateRoute exact path="/my-products">
          <MyProducts />
        </PrivateRoute>
        <Route exact path="/products/:id" children={<Product />} />
        <Route exact path="/producer/:id" children={<Producer />} />
        <PrivateRoute exact path="/product-details-add">
          <ProductDetails />
        </PrivateRoute>
        <Route exact path="/login">
          <Login />
        </Route>
        <Route exact path="/register">
          <Register />
        </Route>

        <PrivateRoute exact path="/shop-cart">
          <ShopCart />
        </PrivateRoute>

        <Route path="*">
          <ErrorComponent />
        </Route>
      </Switch>
      <Footer />
    </div>
  );
}

export default App;
