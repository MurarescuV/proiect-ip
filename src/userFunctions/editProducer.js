import axios from "axios";
import { url } from "../URL";

async function editProducer({
  id,
  clientId,
  firstName,
  lastName,
  phone,
  email,
  password,
  locationId,
  city,
  district,
  country,
  village,
  street,
  bloc,
  stair,
  no,
  document,
  description,
  active,
  validAccount,
  role,
}) {
  let producer;
  if (typeof document === "string") {
    producer = {
      id,
      clientId,
      firstName,
      lastName,
      phone,
      email,
      password,
      locationId,
      city,
      district,
      country,
      village,
      street,
      bloc,
      stair,
      no,
      document,
      description,
      active,
      validAccount,
      role,
    };
  } else {
    let data = new FormData();
    data.append("uploadfile", document);
    let newDocumentUrl = await axios.post(url + "/api/file/upload", data);
    producer = {
      id,
      clientId,
      firstName,
      lastName,
      phone,
      email,
      password,
      locationId,
      city,
      district,
      country,
      village,
      street,
      bloc,
      stair,
      no,
      document: newDocumentUrl.data,
      description,
      active,
      validAccount,
      role,
    };
  }
  // console.log(producer);
  const response = await axios.put(url + `/api/v1/producer`, producer);
  if (response) {
    // console.log(response);
    window.location.reload(true);
  }
}

export default editProducer;
