import axios from "axios";
import { url } from "../URL";

async function initiateOrder({
  clientId,
  confirmed,
  details,
  totalPrice,
  locationId,
  date,
}) {
  let order = {
    clientId,
    confirmed,
    details,
    totalPrice,
    locationId,
    date,
  };

  // console.log("comanda inainte de a fi initializata", order);

  let response = await axios
    .post(url + "/api/v1/order/initiate", order)
    .catch((error) => console.log("eroare la trimitere", error));

  if (response) {
    return response;
  }
}

export default initiateOrder;
