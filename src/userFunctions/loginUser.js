import axios from "axios";
import { url } from "../URL";

async function loginUser({ email, password }) {
  const response = await axios
    .post(url + "/api/v1/auth/signin", { email, password })
    .catch((error) => console.log(error));
  return response;
}

export default loginUser;
