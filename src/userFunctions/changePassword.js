import axios from "axios";
import { url } from "../URL";

async function changePassword({ email }) {
  let data = { email };
  const response = await axios
    .post(url + "/api/v1/auth/reset-password", data)
    .catch((error) => console.log(error));
  //   if (response) {
  //     window.location.reload(true);
  //   }
}

export default changePassword;
