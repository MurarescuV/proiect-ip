import axios from "axios";
import { url } from "../URL";

async function editProduct({
  id,
  producerId,
  vegetableId,
  avaibleQuantity,
  unit,
  image,
  homeDelivery,
  details,
  valid,
  alive,
  pricePerUnit,
}) {
  let product;

  if (typeof image === "string") {
    product = {
      id,
      producerId,
      vegetableId,
      avaibleQuantity,
      unit,
      image,
      homeDelivery,
      details,
      valid,
      alive,
      pricePerUnit: Number(pricePerUnit),
    };
  } else {
    let data = new FormData();
    data.append("uploadfile", image);
    let newImageUrl = await axios.post(url + "/api/file/upload", data);
    product = {
      id,
      producerId,
      vegetableId,
      avaibleQuantity,
      unit,
      image: newImageUrl.data,
      homeDelivery,
      details,
      valid,
      alive,
      pricePerUnit: Number(pricePerUnit),
    };
  }
  axios
    .put(url + `/api/v1/notice`, product)
    .then((response) => {
      if (response.data) {
        window.location.reload(true);
      }
    })
    .catch((error) => console.log(error));
}

export default editProduct;
