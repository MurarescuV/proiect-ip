import axios from "axios";
import { url } from "../URL";

async function singUpClient(client) {
  const response = await axios
    .post(url + "/api/v1/auth/signup/client", client)
    .catch((error) => console.log(error));
  return response;
}

export default singUpClient;
