import axios from "axios";
import { url } from "../URL";

function addProduct({
  producerId,
  vegetableId,
  avaibleQuantity,
  unit,
  image,
  homeDelivery,
  details,
  valid,
  alive,
  pricePerUnit,
}) {
  let data = new FormData();
  data.append("uploadfile", image);

  axios
    .post(url + "/api/file/upload", data)
    .then((response) => {
      let image = response.data;
      let product = {
        producerId,
        vegetableId,
        avaibleQuantity,
        unit,
        image,
        homeDelivery,
        details,
        valid,
        alive,
        pricePerUnit,
      };
      axios
        .post(url + "/api/v1/notice", product)
        .then((response) => {
          if (response.data) {
            window.location.reload(true);
          }
        })
        .catch((error) => console.log(error));
    })
    .catch((err) => console.log(err));
}

export default addProduct;
