import axios from "axios";
import { url } from "../URL";

async function editClient({
  id,
  firstName,
  lastName,
  phone,
  email,
  password,
  locationId,
  city,
  district,
  country,
  village,
  street,
  bloc,
  stair,
  no,
  validAccount,
  role,
}) {
  let client = {
    id,
    firstName,
    lastName,
    phone,
    email,
    password,
    locationId,
    city,
    district,
    country,
    village,
    street,
    bloc,
    stair,
    no,
    validAccount,
    role,
  };
  //   console.log(client);
  const response = await axios.put(url + `/api/v1/client`, client);
  if (response) {
    // console.log(response);
    window.location.reload(true);
  }
}

export default editClient;
