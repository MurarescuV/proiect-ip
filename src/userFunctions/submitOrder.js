import axios from "axios";
import { url } from "../URL";

async function submitOrder() {
  let currentOrderId = JSON.parse(localStorage.getItem("orderId"));
  let response = await axios
    .get(url + `/api/v1/order/sent/orderId/${currentOrderId}`)
    .catch((error) => console.log(error));
  return response;
}

export default submitOrder;
