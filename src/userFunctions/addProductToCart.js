import axios from "axios";
import { url } from "../URL";

async function addProductToCart({ noticeId, quantity, deliveryMode, orderId }) {
  let product = {
    noticeId,
    quantity,
    deliveryMode,
    orderId: orderId,
  };

  // console.log("produseul inainte de a fi adaugat in cos", product);

  let response = await axios
    .post(url + "/api/v1/product", product)
    .catch((error) => console.log(error));
  if (response) {
    // console.log("adaugare produs in cart : ", response);
    return response;
  }
}

export default addProductToCart;
