import axios from "axios";
import { url } from "../URL";

async function singUpProducer({
  firstName,
  lastName,
  phone,
  email,
  password,
  city,
  district,
  country,
  village,
  street,
  bloc,
  stair,
  no,
  document,
  description,
  active,
  validAccount,
  role,
}) {
  let data = new FormData();
  data.append("uploadfile", document);
  let newDocumentUrl = await axios.post(url + "/api/file/upload", data);
  let producer = {
    firstName,
    lastName,
    phone,
    email,
    password,
    city,
    district,
    country,
    village,
    street,
    bloc,
    stair,
    no,
    document: newDocumentUrl.data,
    description,
    active: false,
    validAccount: true,
    role,
  };
  const response = await axios
    .post(url + "/api/v1/auth/signup/producer", producer)
    .catch((error) => console.log(error));
  return response;
}

export default singUpProducer;
