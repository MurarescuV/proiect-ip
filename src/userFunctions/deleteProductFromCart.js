import axios from "axios";
import { url } from "../URL";

async function deleteProductFromCart(id) {
  let response = axios
    .delete(url + `/api/v1/product/id/${id}`)
    .catch((error) => console.log(error));
  if (response) {
    setTimeout(() => {
      window.location.reload(true);
    }, 200);
  }
}

export default deleteProductFromCart;
