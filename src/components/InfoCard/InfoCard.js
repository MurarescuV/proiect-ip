import React from "react";
import "./InfoCard.css";
const InfoCard = (props) => {
  return (
    <div className="info-card-container">
      <div className="info-card-photo">
        <img src={props.img} alt="img" />
      </div>
      <div className="info-card-description">
        <strong style={{ fontSize: "18px" }}>{props.title}</strong> <br />{" "}
        {props.content}
      </div>
    </div>
  );
};

export default InfoCard;
