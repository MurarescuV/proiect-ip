import React from "react";
import "./Button.css";
const Button = (props) => {
  return (
    <button type="submit" className="btn" onClick={props.clickHandle}>
      {props.content}
    </button>
  );
};

export default Button;
