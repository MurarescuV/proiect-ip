import React from "react";
import "./ProductCard.css";

const ProductCard = (props) => {
  return (
    <div className="product-card-container">
      <div className="product-card-photo">
        <img src={props.image} alt="img" />
      </div>
      <div className="product-card-description">
        <h3>{props.vegetableName}</h3>
        <h5>
          {props.producerFirstName} {props.producerLastName}
        </h5>
        <h5>
          {props.pricePerUnit} lei / {props.unit}
        </h5>
        {/* <Button content="Adaugă în coș" />
     
        <Link to="" className="link">
          Detalii
        </Link> */}
        {props.children}
      </div>
    </div>
  );
};

export default ProductCard;
