import React from "react";
import "./SimpleButton.css";

const SimpleButton = (props) => {
  return (
    <button id='myBtn' className="link-button" onClick={props.clickHandle}>
      {props.content}
    </button>
  );
};

export default SimpleButton;
