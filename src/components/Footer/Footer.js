import React from "react";
import "./Footer.css";
const Footer = () => {
  return (
    <div className="grid-container">
      <div className="grid-item">
        <h1>Direcția pentru Agricultură Județeană Suceava</h1>
        <p>Contact </p>
        <p>Telefon: 0230 511039</p>
        <p>Fax: 0230 511040 </p>
        <p>Email: dadr.sv@madr.ro</p>
      </div>
      <div className="grid-item">
        <h1>Copyright © 2021 • DAJS • All rights reserved</h1>
        <p>Powered by USV</p>
        <p>Murărescu Vlad</p>
        <p>Murărescu Sebastian</p>
        <p>Cîrlan Ancuta</p>
        <p>Burlă David</p>
      </div>
      <div className="grid-item">
        <img
          className="img-logo"
          src="https://fyi.extension.wisc.edu/safefood/files/2019/04/CDC_produce.png"
          alt="img"
        />
      </div>
    </div>
  );
};

export default Footer;
