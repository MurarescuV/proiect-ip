import React, { useContext } from "react";
import { Route } from "react-router-dom";
import { UserContext } from "../../context/userContext";
import ErrorComponent from "../ErrorComponent/ErrorComponent";

const PrivateRoute = ({ children, ...props }) => {
  const {
    user: { token },
  } = useContext(UserContext);

  return (
    <Route
      {...props}
      render={() => {
        return token !== null ? children : <ErrorComponent />;
      }}
    ></Route>
  );
};

export default PrivateRoute;
