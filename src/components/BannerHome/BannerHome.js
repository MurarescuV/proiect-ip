import React, { Component } from "react";
import "./BannerHome.css";
import { Link } from "react-router-dom";
class BannerHome extends Component {
  render() {
    return (
      <div className="home-banner">
        <h1>Nu mai umbla pe la tarabe, toate sunt aici!</h1>
        <Link className="home-link" to="/products">
          Vezi anunțurile
        </Link>
      </div>
    );
  }
}

export default BannerHome;
