import React from "react";
import "./ProducerCard.css";
import { Link } from "react-router-dom";
const ProducerCard = (props) => {
  return (
    <div className="producer-card-container">
      <p>
        {props.firstName} {props.lastName}
      </p>
      <Link to={`/producer/${props.id}`} className="link">
        Detalii
      </Link>
    </div>
  );
};

export default ProducerCard;
