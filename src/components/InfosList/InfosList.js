import React from "react";
import "./InfosList.css";
import InfoCard from "../InfoCard/InfoCard";
const InfosList = () => {
  return (
    <div className="info-list-container">
      <InfoCard
        title="Legume "
        content="În fiecare primavară le plantăm pentru a avea mai tarziu."
        img="https://www.vegan.io/blog/assets/10-healthiest-vegetables-to-include-in-your-vegan-diet-2018-04-16/healthiest-vegetables-df1cf550711076d052eaade12c38289a2637c38e546182d3c0136a90cb0bb0b3.jpg"
      />
      <InfoCard
        title="Fructe"
        content="Cu cât mai mult soare, cu atât mai dulce!"
        img="https://www.stiriagricole.ro/wp-content/uploads/2016/09/mere-pere-fructe-1024x768.jpg"
      />
      <InfoCard
        title="Cereale"
        content="Îndeajuns pentru fiecare: calitatea întai omului, iar restul pentru animale."
        img="https://www.charisma.ro/upload/img/contents/img/productie-furajera-1475827912_800x496-1602504719.jpg"
      />
    </div>
  );
};

export default InfosList;
