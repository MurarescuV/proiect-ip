import React, { useContext, useState } from "react";
import "../../pages/Login/Login.css";
import "../../pages/Register/Register";
import SimpleButton from "../SimpleButton/SimpleButton";
import Loading from "../Loading/Loading";
import Title from "../Title/Title";
import { InfosContext } from "../../context/Contex";
import editProducer from "../../userFunctions/editProducer";
import editClient from "../../userFunctions/editClient";

const EditUser = (props) => {
  let {
    locations,
    loadingLocations,
    editProducerInfos,
    editClientInfos,
  } = useContext(InfosContext);

  if (loadingLocations) {
    <Loading />;
  }

  let currentUser =
    props.role === "ROLE_PRODUCATOR" ? editProducerInfos : editClientInfos;

  const [firstName, setFirstName] = useState(currentUser.firstName);
  const [lastName, setLastName] = useState(currentUser.lastName);
  const [email, setEmail] = useState(currentUser.email);
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState(currentUser.phone);
  const [village, setVillage] = useState(currentUser.village);
  const [street, setStreet] = useState(currentUser.street);
  const [bloc, setBloc] = useState(currentUser.bloc);
  const [stair, setStair] = useState(currentUser.stair);
  const [no, setNo] = useState(currentUser.no);
  const [role, setRole] = useState(currentUser.role);
  const [document, setDocument] = useState(currentUser.document);
  const [newDocument, setNewDocument] = useState(null);
  const [description, setDescription] = useState(currentUser.description);
  const [validPass, setValidPass] = useState(true);

  return (
    <>
      <Title title="Editează datele de producător" />
      <div className="register-form-group">
        <form className="register-form">
          <div className="input-group">
            <label htmlFor="lastName">Nume</label>
            <input
              type="text"
              id="lastName"
              name="lastName"
              onChange={(e) => setLastName(e.target.value)}
              value={lastName}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="firstName">Prenume</label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              onChange={(e) => setFirstName(e.target.value)}
              value={firstName}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="username">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="password">Parolă</label>
            <input
              type="password"
              id="password"
              name="password"
              placeholder="Parola curentă / nouă"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="phone">Număr de telefon</label>
            <input
              type="text"
              id="phone"
              name="phone"
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
              required
              autoComplete="off"
              minLength="10"
            />
          </div>
          <div className="input-group">
            <label htmlFor="village">Localitate</label>
            <select
              className="roleSelect"
              name="village"
              id="village"
              onChange={(e) => setVillage(e.target.value)}
              value={village}
            >
              <option value="Selectează">Selectează</option>
              {locations.map((location) => {
                return (
                  <option key={location.id} value={location.diacritice}>
                    {location.diacritice}
                  </option>
                );
              })}

              {/* <option value="Darmanesti">Darmanesti</option>
              <option value="Suceava">Suceava</option> */}
            </select>
          </div>
          <div className="input-group">
            <label htmlFor="street">Strada</label>
            <input
              type="text"
              id="street"
              name="street"
              onChange={(e) => setStreet(e.target.value)}
              value={street}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="bloc">Bloc</label>
            <input
              type="text"
              id="bloc"
              name="bloc"
              onChange={(e) => setBloc(e.target.value)}
              value={bloc}
              required
              autoComplete="off"
            />
          </div>
          <div className="input-group">
            <label htmlFor="stair">Scara</label>
            <input
              type="text"
              id="stair"
              name="stair"
              onChange={(e) => setStair(e.target.value)}
              value={stair}
              required
              autoComplete="off"
            />
          </div>
          <div className="input-group">
            <label htmlFor="no">Numărul</label>
            <input
              type="text"
              id="no"
              name="no"
              onChange={(e) => setNo(e.target.value)}
              value={no}
              required
              autoComplete="off"
            />
          </div>
          {props.role === "ROLE_PRODUCATOR" && (
            <>
              <div className="input-group">
                <label htmlFor="document">Autorizație</label>
                <label
                  style={{
                    fontSize: "13px",
                    letterSpacing: "1px",
                    color: "white",
                  }}
                  className="link-button"
                  htmlFor="document"
                >
                  Incarcă fișier
                  <input
                    type="file"
                    id="document"
                    name="document"
                    onChange={(e) => setNewDocument(e.target.files[0])}
                    autoComplete="off"
                    style={{
                      width: "100%",
                      height: "100%",
                      display: "none",
                    }}
                  />
                </label>
              </div>

              <div className="input-group">
                <label htmlFor="description">Descriere</label>
                <textarea
                  className="register-textarea"
                  name="description"
                  id="description"
                  cols="30"
                  rows="10"
                  onChange={(e) => setDescription(e.target.value)}
                  value={description}
                  required
                  autoComplete="off"
                  minLength="2"
                ></textarea>
              </div>
            </>
          )}

          <div
            className="input-group"
            style={{ justifyContent: "space-around" }}
          >
            <SimpleButton
              content="Salvează modificările"
              clickHandle={(e) => {
                e.preventDefault();
                if (password.length >= 2) {
                  setValidPass(true);
                  if (props.role === "ROLE_PRODUCATOR") {
                    editProducer({
                      id: currentUser.id,
                      clientId: currentUser.clientId,
                      firstName,
                      lastName,
                      phone,
                      email,
                      password,
                      locationId: currentUser.locationId,
                      city: currentUser.city,
                      district: currentUser.district,
                      country: currentUser.country,
                      village,
                      street,
                      bloc,
                      stair,
                      no,
                      validAccount: currentUser.validAccount,
                      role,
                      description,
                      document: newDocument ?? document,
                      active: currentUser.active,
                    });
                  } else {
                    editClient({
                      id: currentUser.id,
                      firstName,
                      lastName,
                      phone,
                      email,
                      password,
                      locationId: currentUser.locationId,
                      city: currentUser.city,
                      district: currentUser.district,
                      country: currentUser.country,
                      village,
                      street,
                      bloc,
                      stair,
                      no,
                      validAccount: currentUser.validAccount,
                      role,
                    });
                  }
                } else {
                  setValidPass(false);
                }
              }}
            />
          </div>
          <div
            className="input-group"
            style={{ justifyContent: "space-around" }}
          >
            <SimpleButton
              content="Anulează"
              clickHandle={() => {
                window.location.reload(true);
              }}
            />
          </div>
          {!validPass && (
            <p className="register-message" style={{ color: "#000" }}>
              Parolă necompletată!
            </p>
          )}
        </form>
      </div>
    </>
  );
};

export default EditUser;
