import React, { useState, useContext } from "react";
import "../../pages/ProductDetails/ProductDetails.css";
import Loading from "../Loading/Loading";
import { InfosContext } from "../../context/Contex";
import { UserContext } from "../../context/userContext";
import Button from "../Button/Button";
import SimpleButton from "../SimpleButton/SimpleButton";
import editProduct from "../../userFunctions/editProduct";
const EditProduct = () => {
  let {
    loadingAliments,
    loadingProducers,
    aliments,
    editProductInfos,
  } = useContext(InfosContext);
  let [vegetableId, setVegetableId] = useState(editProductInfos.vegetableId);
  let [avaibleQuantity, setAvaibleQuantity] = useState(
    editProductInfos.avaibleQuantity
  );
  let [unit, setUnit] = useState(editProductInfos.unit);
  let [image, setImage] = useState(editProductInfos.image);
  let [newImage, setNewImage] = useState(null);
  let [homeDelivery, setHomeDelivery] = useState(editProductInfos.homeDelivery);
  let [details, setDetails] = useState(editProductInfos.details);
  let [pricePerUnit, setPricePerUnit] = useState(editProductInfos.pricePerUnit);

  let { user } = useContext(UserContext);
  //   let { loadingAliments, loadingProducers, aliments, producers } = useContext(
  //     InfosContext
  //   );
  if (
    loadingAliments &&
    loadingProducers &&
    (user.email === undefined || user.email === null)
  ) {
    return <Loading />;
  }

  // let currentProducer = producers.find((producer) => {
  //   return producer.email === user.email;
  // });
  return (
    <>
      <form className="product-details-form-group">
        <div className="product-details-input-group">
          <label htmlFor="product-name">Denumire produs</label>
          <select
            className="roleSelect"
            name="product-name"
            id="product-name"
            onChange={(e) => setVegetableId(e.target.value)}
            value={vegetableId}
          >
            <option value="Selectează">Selectează</option>
            {aliments.map((aliment) => {
              return (
                <option key={aliment.id} value={aliment.id}>
                  {aliment.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="available-quantity">Cantitate disponibilă</label>
          <input
            type="text"
            id="available-quantity"
            name="avaibleQuanitty"
            onChange={(e) => setAvaibleQuantity(e.target.value)}
            value={avaibleQuantity}
          />
        </div>
        <div className="product-details-input-group">
          <label htmlFor="unit">Unitate de măsură</label>
          <input
            type="text"
            id="unit"
            name="unit"
            onChange={(e) => setUnit(e.target.value)}
            value={unit}
          />
        </div>
        <div className="product-details-input-group">
          <label htmlFor="image">Imagine</label>
          <label
            style={{ fontSize: "13px", letterSpacing: "1px", color: "white" }}
            className="link-button"
            htmlFor="image"
          >
            Incarcă fișier
            <input
              type="file"
              id="image"
              name="image"
              onChange={(e) => setImage(e.target.files[0])}
              style={{
                width: "100%",
                height: "100%",
                display: "none",
              }}
            />
          </label>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="home-delivery">Livrare la domiciliu</label>
          <select
            className="roleSelect"
            name="homeDelivery"
            id="home-delivery"
            onChange={(e) => setHomeDelivery(e.target.value)}
            value={homeDelivery}
          >
            <option value={true}>Da</option>
            <option value={false}>Nu</option>
          </select>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="details">Informații suplimentare</label>
          {/* <input
            type="text"
            id="details"
            name="details"
            onChange={(e) => setDetails(e.target.value)}
            value={details}
          /> */}
          <textarea
            className="register-textarea"
            name="details"
            id="details"
            cols="30"
            rows="5"
            onChange={(e) => setDetails(e.target.value)}
            value={details}
          ></textarea>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="price-per-unit">Preț pe unitate</label>
          <input
            type="text"
            id="price-per-unit"
            name="pricePerUnit"
            onChange={(e) => setPricePerUnit(e.target.value)}
            value={pricePerUnit}
          />
        </div>
      </form>
      <div className="product-details-form-group">
        <div className="product-details-input-group">
          <Button
            content="Salvează modificările"
            clickHandle={() => {
              editProduct({
                id: editProductInfos.id,
                producerId: editProductInfos.producerId,
                vegetableId,
                avaibleQuantity,
                unit,
                image: newImage ?? image,
                homeDelivery,
                details,
                valid: editProductInfos.valid,
                alive: editProductInfos.alive,
                pricePerUnit,
              });
            }}
          />
        </div>
        <div className="product-details-input-group">
          <SimpleButton
            content="Anulează"
            clickHandle={() => {
              window.location.reload(true);
            }}
          />
        </div>
      </div>
    </>
  );
};

export default EditProduct;
