import React, { useContext } from "react";
import { Link } from "react-router-dom";
import "./Navbar.css";
import { UserContext } from "../../context/userContext";
import { InfosContext } from "../../context/Contex";
const Navbar = () => {
  let { userLogout, user } = useContext(UserContext);
  let { clearLocalStorage, cartProductsList } = useContext(InfosContext);

  return (
    <nav>
      <div className="logo">
        <Link to="/">
          <p>LaȚărani.ro</p>
        </Link>
      </div>
      <input type="checkbox" id="click" />
      <label htmlFor="click" className="menu-btn">
        <i className="fas fa-bars"></i>
      </label>
      <ul>
        <li>
          <Link to="/" className="a">
            Acasă
          </Link>
        </li>
        <li>
          <Link to="/indications" className="a">
            Indicații
          </Link>
        </li>
        <li>
          <Link to="/products" className="a">
            Anunțuri
          </Link>
        </li>
        {user.token !== null && (
          <li>
            <Link to="/my-products" className="a">
              Cont
            </Link>
          </li>
        )}

        {user.token === null && (
          <li>
            <Link to="/login" className="a">
              Logare
            </Link>
          </li>
        )}

        {user.token === null && (
          <li>
            <Link to="/register" className="a">
              Înregistrare
            </Link>
          </li>
        )}

        {user.token !== null && (
          <li className="dropdown">
            <Link to="/shop-cart" className="a">
              <i className="fas fa-shopping-cart cart"></i>
            </Link>
            <div className="dropdown-content">
              <div className="shop-cart-item">
                <div className="dropdown-shop-cart-total">
                  <div className="dropdown-cart-price">
                    <p style={{ color: "white" }}>
                      {cartProductsList.length !== 0
                        ? "Aveți produse în coș."
                        : "Coșul dumneavoastră este gol."}
                    </p>
                  </div>
                  <hr></hr>
                  <Link to="/shop-cart">
                    <button className="dropdown-shop-cart-button">
                      Către coș
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          </li>
        )}

        {user.token !== null && (
          <li>
            <Link
              to="/"
              className="a"
              onClick={() => {
                userLogout();
                clearLocalStorage();
              }}
            >
              Delogare
            </Link>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default Navbar;
