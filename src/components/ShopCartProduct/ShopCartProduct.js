import React, { useState, useContext, useEffect } from "react";
import "./ShopCartProduct.css";
import { InfosContext } from "../../context/Contex";
import axios from "axios";
import Loading from "../Loading/Loading";
import deleteProductFromCart from "../../userFunctions/deleteProductFromCart";
import { url } from "../../URL";

const ShopCartProduct = (props) => {
  let [cartProducts, setCartProudcts] = useState([]);
  let [loading, setLoading] = useState(true);
  let { cartProductsList } = useContext(InfosContext);
  let orderId = JSON.parse(localStorage.getItem("orderId"));
  // console.log("order id", orderId);

  useEffect(() => {
    setLoading(true);
    axios
      .get(url + `/api/v1/product/orderId/${orderId}`)
      .then((response) => {
        setCartProudcts(response.data);
      })
      .catch((error) => console.log(error));
    setLoading(false);
  }, [cartProductsList, orderId]);

  if (loading) {
    return <Loading />;
  }

  let currentProd = cartProducts.find(
    (product) => product.noticeId === props.id
  );

  if ((loading && currentProd === null) || currentProd === undefined) {
    return <Loading />;
  }

  // console.log(currentProd);

  let { id } = currentProd;

  return (
    <div className="shop-cart">
      <div className="shop-cart-image">
        <img className="shop-cart-image" src={props.img} alt="img" />
      </div>
      <div className="shop-cart-mid">
        <div>
          <p>Nume produs:</p>
          <p>Preț:</p>
          <p>Cantitate:</p>
          <p>Producător:</p>
        </div>
        <div>
          <p>{props.name}</p>
          <p>
            {props.price} lei / {props.unit}
          </p>
          <p>
            {props.quantity} {props.unit}
          </p>
          <p>{props.producer}</p>
        </div>
      </div>
      <div className="shop-cart-close">
        <div
          className="modal-delete-text"
          onClick={() => {
            deleteProductFromCart(id);
            props.clickHandle();
          }}
        >
          Șterge
        </div>
      </div>
    </div>
  );
};

export default ShopCartProduct;

// class ShopCartProduct extends Component {
//   render() {
//     return (
//       <div className="shop-cart">
//         <div className="shop-cart-image">
//           <img className="shop-cart-image" src={this.props.img} alt="img" />
//         </div>
//         <div className="shop-cart-mid">
//           <div>
//             <p>Nume produs:</p>
//             <p>Preț:</p>
//             <p>Cantitate:</p>
//             <p>Producător:</p>
//           </div>
//           <div>
//             <p>{this.props.name}</p>
//             <p>
//               {this.props.price} lei / {this.props.unit}
//             </p>
//             <p>
//               {this.props.quantity} {this.props.unit}
//             </p>
//             <p>{this.props.producer}</p>
//           </div>
//         </div>
//         <div className="shop-cart-close">
//           <div className="modal-delete-text" onClick={this.props.clickHandle}>
//             Șterge
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default ShopCartProduct;
