import React from "react";
import "./ProductsList.css";
const ProductsList = (props) => {
  return <div className="products-list-container">{props.children}</div>;
};

export default ProductsList;
