import React, { useContext, useState } from "react";
import SimpleButton from "../SimpleButton/SimpleButton";
import "./Modal.css";
import { InfosContext } from "../../context/Contex";
import { UserContext } from "../../context/userContext";
import Loading from "../Loading/Loading";
import initiateOrder from "../../userFunctions/initiateOrder";
import addProductToCart from "../../userFunctions/addProductToCart";
const Modal = (props) => {
  let [quantityInput, setQuantityInput] = useState(1);
  let [isAdded, setIsAdded] = useState(false);
  let {
    loadingProducts,
    products,
    loadingProducers,
    producers,
    clients,
    setOrder,
    orderStatus,
    setOrderId,
    setOderStatus,
  } = useContext(InfosContext);
  let { user } = useContext(UserContext);

  // console.log("user", user);

  let currentOrderId;

  if (
    loadingProducts ||
    loadingProducers ||
    user === undefined ||
    user === null
  ) {
    return <Loading />;
  }

  const initiateOrderHandle = async (user, product, quantity) => {
    let currentUser;
    if (user.role === "ROLE_CUMPARATOR") {
      currentUser = clients.find((client) => client.email === user.email);
    } else {
      currentUser = producers.find((producer) => producer.email === user.email);
    }
    // console.log(currentUser);
    if (orderStatus.isInitiate === false) {
      let response = await initiateOrder({
        clientId:
          currentUser.role === "ROLE_CUMPARATOR"
            ? currentUser.id
            : currentUser.clientId,
        confirmed: false,
        details:
          "Pentru informații suplimentare, contactați producătorul/producătorii. Vă mulțumim!",
        totalPrice: 0,
        locationId: user.locationId,
        date: new Date("2015-03-25T12:00:00"),
      });
      if (response) {
        // console.log("comanda initializata are id ul: ", response.data.id);
        setOrderId(response.data.id);
        // console.log("comanda a fost initializata");
        setOderStatus();
      }
    }
    currentOrderId = JSON.parse(localStorage.getItem("orderId"));
    setIsAdded(false);
    // console.log("current order id : ", currentOrderId);
    let response = await addProductToCart({
      noticeId: product.id,
      quantity,
      deliveryMode: product.homeDelivery === true ? "home" : "no",
      orderId: currentOrderId,
    });
    if (response) {
      // console.log("introducere a produsului cu succes");
      setIsAdded(true);
    }
  };

  let currentUser;
  if (user.role === "ROLE_CUMPARATOR") {
    currentUser = clients.find((client) => client.email === user.email);
  } else {
    currentUser = producers.find((producer) => producer.email === user.email);
  }

  let currentProduct = products.find((product) => product.id === props.id);

  // console.log(currentUser);
  // console.log("produsul curent pregatit pentru comanda:", currentProduct);
  // console.log("userul current care initializeaza comanda:", currentUser);
  return (
    <>
      <div id="myModal" className="modal">
        <div className="modal-content">
          <button onClick={props.clickCancel} className="close">
            &times;
          </button>
          <p className="modal-text">Adăugați cantitatea dorită</p>
          <input
            type="number"
            name="quantityInput"
            value={quantityInput}
            onChange={(e) => setQuantityInput(e.target.value)}
            min="1"
            max={`${currentProduct.avaibleQuantity}`}
          />

          <SimpleButton
            clickHandle={() => {
              initiateOrderHandle(currentUser, currentProduct, quantityInput);
              setOrder(
                currentUser.role === "ROLE_CUMPARATOR"
                  ? currentUser.id
                  : currentUser.clientId,
                currentProduct.id,
                quantityInput,
                currentProduct.homeDelivery,
                1,
                currentProduct
              );
            }}
            content="Adaugă"
          />
          {isAdded && <p className="modal-text">Produs adăugat în coș</p>}
        </div>
      </div>
    </>
  );
};

export default Modal;
