import React from "react";
import "./PhotoContainer.css";
const PhotoContainer = (props) => {
  return (
    <div className="photo-container">
      <img src={props.src} alt="img" />
    </div>
  );
};

export default PhotoContainer;
