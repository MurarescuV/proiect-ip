import React from "react";
import "./HomeDetails.css";
const HomeDetails = (props) => {
  return (
    <div className="home-details-container">
      <p>{props.firstContent}</p>
      <p>{props.secondContent}</p>
    </div>
  );
};

export default HomeDetails;
