import React, { useContext } from "react";
import "./Filters.css";
import { InfosContext } from "../../context/Contex";
import Loading from "../Loading/Loading";
import Button from "../Button/Button";

const getUnique = (items, value) => {
  return [...new Set(items.map((item) => item[value]))];
};

const Filters = () => {
  let {
    loadingProducts,
    loadingProducers,
    loadingProductLocations,
    loadingAliments,
    products,
    producers,
    productLocations,
    aliments,
    resetFiltersHandle,
    inputFiltersHandle,
    filters,
  } = useContext(InfosContext);

  if (
    loadingProducts ||
    loadingProducers ||
    loadingProductLocations ||
    loadingAliments
  ) {
    return <Loading />;
  }

  // let productNames = products.map((product) => {
  //   return { id: product.id, name: product.vegetableName };
  // });

  let productNames = getUnique(products, "vegetableName");
  productNames.sort();
  productNames = productNames.map((productName) => {
    let productDetails = products.find(
      (product) => product.vegetableName === productName
    );
    return productDetails;
  });

  // console.log("dasdasd", productNames);
  let locations = getUnique(productLocations, "village");
  locations = locations.map((location) => {
    let locationsDetails = productLocations.find(
      (productLocation) => productLocation.village === location
    );
    return locationsDetails;
  });

  ///////

  locations = locations.filter((location) => {
    let isValid = producers.find(
      (producer) => producer.locationId === location.id
    );
    return isValid;
  });

  ///////////

  locations.sort((a, b) =>
    a.village > b.village ? 1 : b.village > a.village ? -1 : 0
  );

  producers.sort((a, b) =>
    a.firstName > b.firstName ? 1 : b.firstName > a.firstName ? -1 : 0
  );
  producers.sort((a, b) =>
    a.lastName > b.lastName ? 1 : b.lastName > a.lastName ? -1 : 0
  );

  let uniqueCategories = getUnique(products, "vegetableId");

  uniqueCategories = uniqueCategories.map((uniqueCategory) => {
    let categoryDetails = aliments.find(
      (aliment) => aliment.id === uniqueCategory
    );
    return categoryDetails;
  });

  uniqueCategories = uniqueCategories.filter(
    (uniqueCategory, index, arr) =>
      index === arr.findIndex((t) => t.category === uniqueCategory.category)
  );

  // console.log("producatori pt filtre: ", producers);
  // console.log("anunturi pt filtre: ", productNames);
  // console.log("locatii pt filtre: ", locations);

  return (
    <form className="form-group">
      <div className="filter-group">
        <label htmlFor="search-product-input">Caută produs</label>
        {/* <input
          type="text"
          id="search-product-input"
          name="searchProduct"
          className="filter-input"
        /> */}
        {/* <div className="icon">
          <i className="fas fa-search"></i>
        </div> */}
        <select
          name="productNameFilter"
          id="search-product-input"
          onChange={inputFiltersHandle}
          value={filters.productNameFilter}
        >
          <option value="Toate">Toate</option>
          {productNames.map((productName) => (
            <option key={productName.id} value={productName.vegetableId}>
              {productName.vegetableName}
            </option>
          ))}
        </select>
      </div>
      <div className="filter-group">
        <label htmlFor="search-producer-input">Caută producător</label>
        {/* <input
          type="text"
          id="search-producer-input"
          name="searchProducer"
          className="filter-input"
        />
        <div className="icon">
          <i className="fas fa-search"></i>
        </div> */}
        <select
          name="producerNameFilter"
          id="search-producer-input"
          value={filters.producerNameFilter}
          onChange={inputFiltersHandle}
        >
          <option value="Toate">Toate</option>
          {producers.map((producer) => (
            <option
              key={producer.id}
              value={producer.lastName}
            >{`${producer.firstName} ${producer.lastName}`}</option>
          ))}
        </select>
      </div>
      <div className="filter-group">
        <label htmlFor="select-category">Categorie</label>
        <select
          name="categoryFilter"
          id="select-category"
          value={filters.categoryFilter}
          onChange={inputFiltersHandle}
        >
          <option value="Toate">Toate</option>
          {uniqueCategories.map((uniqueCategory) => (
            <option key={uniqueCategory.id} value={uniqueCategory.id}>
              {uniqueCategory.category}
            </option>
          ))}
        </select>
      </div>
      <div className="filter-group">
        <label htmlFor="select-location">Locație</label>
        <select
          name="locationFilter"
          id="select-location"
          value={filters.locationFilter}
          onChange={inputFiltersHandle}
        >
          <option value="Toate">Toate</option>
          {locations.map((location, index) => (
            <option key={index} value={location.village}>
              {location.village}
            </option>
          ))}
        </select>
      </div>
      <div className="filter-group">
        <label htmlFor="price">Preț</label>
        {/* <div className="price-inputs">
          <input
            type="number"
            name="minPrice"
            id="price"
            className="price-input filter-input"
          />
          <span> minim</span>
          <input
            type="number"
            name="maxPrice"
            id="price"
            className="price-input filter-input"
          />
          <span> maxim</span>
        </div> */}
        <p>Până la {`${filters.priceFilter}`} lei</p>
        <input
          type="range"
          name="priceFilter"
          min={filters.minPrice}
          max={filters.maxPrice}
          className="price-input"
          value={filters.priceFilter}
          onChange={inputFiltersHandle}
        />
      </div>
      <div className="filter-group">
        <label htmlFor="delivery">Livrare la domiciliu</label>
        <input
          type="checkbox"
          name="homeDeliveryFilter"
          id="delivery"
          className="check-input"
          value={filters.homeDeliveryFilter}
          onChange={inputFiltersHandle}
        />
      </div>
      <div className="filter-group">
        <Button
          content="Elimină filtrele"
          clickHandle={(e) => {
            e.preventDefault();
            resetFiltersHandle();
          }}
        />
      </div>
    </form>
  );
};

export default Filters;
