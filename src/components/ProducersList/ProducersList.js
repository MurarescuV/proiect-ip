import React, { useContext } from "react";
import "./ProducersList.css";
import ProducerCard from "../ProducerCard/ProducerCard";
import Loading from "../Loading/Loading";
import { InfosContext } from "../../context/Contex";
import Title from "../Title/Title";
const ProducersList = () => {
  let { producers, loadingProducers } = useContext(InfosContext);

  if (loadingProducers) {
    return <Loading />;
  }

  return (
    <div className="producers-list-container">
      {producers.length === 0 ? (
        <Title title="..." />
      ) : (
        producers.map((producer) => {
          return (
            <ProducerCard
              key={producer.id}
              id={producer.id}
              firstName={producer.firstName}
              lastName={producer.lastName}
            />
          );
        })
      )}

      {/* <ProducerCard />
      <ProducerCard />
      <ProducerCard />
      <ProducerCard />
      <ProducerCard /> */}
    </div>
  );
};

export default ProducersList;
