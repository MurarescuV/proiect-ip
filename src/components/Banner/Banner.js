import React, { Component } from "react";
import "./Banner.css";
class Banner extends Component {
  render() {
    return (
      <div className="banner">
        <img src={this.props.src} alt="" />
        <h1>{this.props.content}</h1>
      </div>
    );
  }
}

export default Banner;
