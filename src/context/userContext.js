import React, { Component, createContext } from "react";

export const UserContext = createContext();

function getUserFromLocalStorage() {
  return localStorage.getItem("user")
    ? JSON.parse(localStorage.getItem("user"))
    : { email: null, token: null, role: null };
}

export default class UserProvider extends Component {
  state = {
    user: getUserFromLocalStorage(),
    isLogin: false,
  };

  userLogin = (user) => {
    this.setState({
      ...this.state,
      user,
      isLogin: true,
    });
    localStorage.setItem("user", JSON.stringify(user));
  };

  userLogout = () => {
    this.setState({
      ...this.state,
      user: {
        email: null,
        token: null,
        role: null,
      },
      isLogin: false,
    });
    localStorage.removeItem("user");
  };

  render() {
    return (
      <UserContext.Provider
        value={{
          ...this.state,
          userLogin: this.userLogin,
          userLogout: this.userLogout,
        }}
      >
        {this.props.children}
      </UserContext.Provider>
    );
  }
}
