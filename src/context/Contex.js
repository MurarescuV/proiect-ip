import React, { Component, createContext } from "react";
import axios from "axios";
import { url } from "../URL";

export let InfosContext = createContext();

function getOrderFromLocalStorage() {
  if (localStorage.getItem("order") === null) {
    localStorage.setItem("order", JSON.stringify({ clientId: null }));
  }
  return JSON.parse(localStorage.getItem("order"));
}

function getCartProductsFromLocalStorage() {
  if (localStorage.getItem("productsList") === null) {
    localStorage.setItem("productsList", JSON.stringify([]));
  }
  return JSON.parse(localStorage.getItem("productsList"));
}

function getCartProductsListFromLocalStorage() {
  if (localStorage.getItem("cartProductsList") === null) {
    localStorage.setItem("cartProductsList", JSON.stringify([]));
  }
  return JSON.parse(localStorage.getItem("cartProductsList"));
}

function getOrderStatusFromLocalStorage() {
  if (localStorage.getItem("orderStatus") === null) {
    localStorage.setItem("orderStatus", JSON.stringify({ isInitiate: false }));
  }
  return JSON.parse(localStorage.getItem("orderStatus"));
}

function getOrderIdFromLocalStorage() {
  if (localStorage.getItem("orderId") === null) {
    localStorage.setItem("orderId", JSON.stringify(null));
  }
  return JSON.parse(localStorage.getItem("orderId"));
}

export default class InfosProvider extends Component {
  state = {
    locations: [],
    loadingLocations: true,
    products: [],
    loadingProducts: true,
    filteredProducts: [],
    loadingFilteredProducts: true,
    producers: [],
    loadingProducers: true,
    clients: [],
    loadingClients: true,
    aliments: [],
    loadingAliments: true,
    productLocations: [],
    loadingProductLocations: true,
    showEditProduct: false,
    editProductInfos: {
      id: "",
      producerId: "",
      vegetableId: "Selectează",
      avaibleQuantity: "",
      unit: "",
      image: "",
      homeDelivery: true,
      details: "",
      valid: true,
      alive: true,
      pricePerUnit: "",
    },
    showEditUser: false,
    editProducerInfos: {
      id: "",
      clientId: "",
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      password: "",
      locationId: "",
      city: "Suceava",
      district: "Suceava",
      country: "Romania",
      village: "",
      street: "",
      bloc: "",
      stair: "",
      no: "",
      document: "",
      description: "",
      active: true,
      validAccount: true,
      role: "ROLE_PRODUCATOR",
    },
    editClientInfos: {
      id: "",
      firstName: "",
      lastName: "",
      phone: "",
      email: "",
      password: "",
      locationId: "",
      city: "Suceava",
      district: "Suceava",
      country: "Romania",
      village: "",
      street: "",
      bloc: "",
      stair: "",
      no: "",
      validAccount: true,
      role: "ROLE_CUMPARATOR",
    },
    filters: {
      productNameFilter: "Toate",
      producerNameFilter: "Toate",
      categoryFilter: "Toate",
      locationFilter: "Toate",
      homeDeliveryFilter: false,
      priceFilter: 0,
      minPrice: 0,
      maxPrice: 0,
    },
    order: getOrderFromLocalStorage(),
    cartProducts: getCartProductsFromLocalStorage(),
    cartProductsList: getCartProductsListFromLocalStorage(),
    orderStatus: getOrderStatusFromLocalStorage(),
    orderId: getOrderIdFromLocalStorage(),
  };

  componentDidMount() {
    this.setState({
      ...this.state,
      loadingLocations: true,
      loadingProducts: true,
      loadingProducers: true,
      loadingAliments: true,
      loadingClients: true,
      loadingProductLocations: true,
    });
    axios
      .get(
        "https://raw.githubusercontent.com/catalin87/baza-de-date-localitati-romania/master/date/localitati.json"
      )
      .then((response) => {
        let preLocations = response.data;
        let currentLocations = preLocations.filter(
          (item) => item.judet === "Suceava"
        );

        currentLocations.sort((a, b) =>
          a.nume > b.nume ? 1 : b.nume > a.nume ? -1 : 0
        );
        this.setState({
          ...this.state,
          locations: currentLocations,
          loadingLocations: false,
        });
        // console.log("currentLocations: ",currentLocations);
        //comentariu
      })
      .catch((error) => {
        console.log(error);
      });

    axios
      .get(url + "/api/v1/notice")
      .then((response) => {
        // console.log("anunturi", response.data);
        let allProducts = response.data;
        let aliveProducts = allProducts.filter((product) => {
          return product.alive === true && product.valid === true;
        });
        let prices = aliveProducts.map((aliveProduct) =>
          parseInt(aliveProduct.pricePerUnit)
        );
        // console.log("tip de pret: ", typeof prices[0]);
        let maxPrice = Math.max(...prices);
        // console.log("preturi: ", prices);
        // console.log("pretul maxim: ", maxPrice);

        this.setState({
          ...this.state,
          products: aliveProducts,
          loadingProducts: false,
          filteredProducts: aliveProducts,
          loadingFilteredProducts: false,
          filters: {
            ...this.state.filters,
            priceFilter: prices.length === 0 ? 0 : maxPrice,
            maxPrice: maxPrice,
          },
        });
      })
      .catch((error) => {
        console.log(error);
      });

    axios
      .get(url + "/api/v1/producer")
      .then((response) => {
        let allProducers = response.data;
        this.setState({
          ...this.state,
          producers: allProducers,
          loadingProducers: false,
        });
      })
      .catch((error) => {
        console.log(error);
      });

    axios
      .get(url + "/api/v1/client/role?role=ROLE_CUMPARATOR")
      .then((response) => {
        let allClients = response.data;
        this.setState({
          ...this.state,
          clients: allClients,
          loadingClients: false,
        });
      })
      .catch((error) => console.log(error));

    axios.get(url + "/api/v1/vegetable").then((response) => {
      let allAliments = response.data;
      allAliments.sort((a, b) =>
        a.name > b.name ? 1 : b.name > a.name ? -1 : 0
      );
      this.setState({
        ...this.state,
        aliments: allAliments,
        loadingAliments: false,
      });
    });
    axios
      .get(url + "/api/v1/location")
      .then((response) => {
        let allProductLocations = response.data;
        this.setState({
          ...this.state,
          productLocations: allProductLocations,
          loadingProductLocations: false,
        });
      })
      .catch((error) => console.log(error));
  }

  setEditProductInfos = ({
    id,
    producerId,
    vegetableId,
    avaibleQuantity,
    unit,
    image,
    homeDelivery,
    details,
    valid,
    alive,
    pricePerUnit,
  }) => {
    let product = {
      id,
      producerId,
      vegetableId,
      avaibleQuantity,
      unit,
      image,
      homeDelivery,
      details,
      valid,
      alive,
      pricePerUnit,
    };
    this.setState({
      ...this.state,
      editProductInfos: product,
      showEditProduct: !this.showEditProduct,
    });
  };

  setEditProducerInfos = ({
    id,
    clientId,
    firstName,
    lastName,
    phone,
    email,
    password,
    locationId,
    city,
    district,
    country,
    village,
    street,
    bloc,
    stair,
    no,
    document,
    description,
    active,
    validAccount,
    role,
  }) => {
    let producer = {
      id,
      clientId,
      firstName,
      lastName,
      phone,
      email,
      password,
      locationId,
      city,
      district,
      country,
      village,
      street,
      bloc,
      stair,
      no,
      document,
      description,
      active,
      validAccount,
      role,
    };
    this.setState({
      ...this.state,
      editProducerInfos: producer,
      showEditUser: !this.state.showEditUser,
    });
  };

  setEditClientInfos = ({
    id,
    firstName,
    lastName,
    phone,
    email,
    password,
    locationId,
    city,
    district,
    country,
    village,
    street,
    bloc,
    stair,
    no,
    validAccount,
    role,
  }) => {
    let client = {
      id,
      firstName,
      lastName,
      phone,
      email,
      password,
      locationId,
      city,
      district,
      country,
      village,
      street,
      bloc,
      stair,
      no,
      validAccount,
      role,
    };
    this.setState({
      ...this.state,
      editClientInfos: client,
      showEditUser: !this.state.showEditUser,
    });
  };

  inputFiltersHandle = (e) => {
    let name = e.target.name;
    let value =
      e.target.type === "checkbox" ? e.target.checked : e.target.value;
    this.setState(
      {
        ...this.state,
        filters: { ...this.state.filters, [name]: value },
      },
      this.filterChange
    );
  };

  filterChange = () => {
    let {
      products,
      filters,
      aliments,
      producers,
      productLocations,
    } = this.state;
    let {
      productNameFilter,
      producerNameFilter,
      categoryFilter,
      locationFilter,
      priceFilter,
      homeDeliveryFilter,
    } = filters;
    let currentProducts = [...products];
    if (productNameFilter !== "Toate") {
      currentProducts = currentProducts.filter(
        (product) => product.vegetableId === parseInt(productNameFilter)
      );
    }

    if (producerNameFilter !== "Toate") {
      currentProducts = currentProducts.filter(
        (product) => product.producerLastName === producerNameFilter
      );
    }

    if (categoryFilter !== "Toate") {
      let currentAliment = aliments.find(
        (aliment) => aliment.id === parseInt(categoryFilter)
      );
      // console.log("alimentul curent", currentAliment);
      let currentAliments = aliments.filter(
        (aliment) => aliment.category === currentAliment.category
      );

      currentProducts = currentProducts.filter((product) => {
        let isValid = currentAliments.find(
          (aliment) => aliment.id === product.vegetableId
        );
        return isValid;
      });
    }

    if (locationFilter !== "Toate") {
      // console.log("location filter: ", locationFilter);
      // console.log("locations filter: ", productLocations);
      let currentLocations = productLocations.filter(
        (location) => location.village === locationFilter
      );
      // console.log("current locations from FILTER", currentLocations);
      let currentProducers = producers.filter((producer) => {
        let isValid = currentLocations.find(
          (location) => location.id === producer.locationId
        );
        // console.log("current producers from FILTER", isValid);
        return isValid;
      });
      // console.log("current producers FILTER", currentProducers);

      currentProducts = currentProducts.filter((product) => {
        let isValid = currentProducers.find(
          (producer) => producer.id === product.producerId
        );
        // console.log("current products from FILTER", isValid);
        return isValid;
      });
    }

    currentProducts = currentProducts.filter(
      (product) => product.pricePerUnit <= priceFilter
    );

    if (homeDeliveryFilter !== false) {
      currentProducts = currentProducts.filter(
        (product) => product.homeDelivery === homeDeliveryFilter
      );
    }

    this.setState({ ...this.state, filteredProducts: currentProducts });
  };

  resetFiltersHandle = () => {
    window.location.reload(true);
  };

  setOrder = (clientId, noticeId, quantity, deliveryMode, orderId, product) => {
    let currentOrder = { clientId: clientId };
    localStorage.setItem("order", JSON.stringify(currentOrder));

    let currentList = getCartProductsFromLocalStorage();
    currentList.push({
      noticeId,
      quantity,
      deliveryMode: deliveryMode === true ? "home" : "no",
      orderId,
    });
    localStorage.setItem("productsList", JSON.stringify(currentList));

    let currentProductsList = getCartProductsListFromLocalStorage();
    currentProductsList.push({ ...product, quantity });
    localStorage.setItem(
      "cartProductsList",
      JSON.stringify(currentProductsList)
    );

    this.setState({
      ...this.state,
      order: getOrderFromLocalStorage(),
      cartProducts: getCartProductsFromLocalStorage(),
      cartProductsList: getCartProductsListFromLocalStorage(),
    });
  };

  setOderStatus = () => {
    let currentOrderStatus = getOrderStatusFromLocalStorage();
    currentOrderStatus = true;
    localStorage.setItem("orderStatus", JSON.stringify(currentOrderStatus));
    this.setState({
      ...this.state,
      orderStatus: getOrderStatusFromLocalStorage(),
    });
  };

  removeProductFromCart = (product) => {
    let currentCartProducts = getCartProductsFromLocalStorage();
    currentCartProducts = currentCartProducts.filter(
      (cartProduct) => cartProduct.noticeId !== product.id
    );
    localStorage.setItem("productsList", JSON.stringify(currentCartProducts));

    let currentCartProductsList = getCartProductsListFromLocalStorage();
    currentCartProductsList = currentCartProductsList.filter(
      (cartProduct) => cartProduct.id !== product.id
    );
    localStorage.setItem(
      "cartProductsList",
      JSON.stringify(currentCartProductsList)
    );
    this.setState({
      ...this.state,
      cartProducts: getCartProductsFromLocalStorage(),
      cartProductsList: getCartProductsListFromLocalStorage(),
    });
  };

  setOrderId = (id) => {
    let currentOrderId = getOrderIdFromLocalStorage();
    currentOrderId = id;
    localStorage.setItem("orderId", JSON.stringify(currentOrderId));
    this.setState({ ...this.state, orderId: getOrderIdFromLocalStorage() });
  };

  clearLocalStorage = () => {
    localStorage.removeItem("order");
    localStorage.removeItem("productsList");
    localStorage.removeItem("cartProductsList");
    localStorage.removeItem("orderStatus");
    localStorage.removeItem("orderId");
    this.setState({
      ...this.state,
      order: getOrderFromLocalStorage(),
      cartProducts: getCartProductsFromLocalStorage(),
      cartProductsList: getCartProductsListFromLocalStorage(),
      orderStatus: getOrderStatusFromLocalStorage(),
      orderId: getOrderIdFromLocalStorage(),
    });
  };

  render() {
    // console.log("locations: ", this.state.locations);
    return (
      <InfosContext.Provider
        value={{
          ...this.state,

          setEditProductInfos: this.setEditProductInfos,
          setEditProducerInfos: this.setEditProducerInfos,
          setEditClientInfos: this.setEditClientInfos,
          resetFiltersHandle: this.resetFiltersHandle,
          inputFiltersHandle: this.inputFiltersHandle,
          setOrder: this.setOrder,
          clearLocalStorage: this.clearLocalStorage,
          removeProductFromCart: this.removeProductFromCart,
          setOrderId: this.setOrderId,
          setOderStatus: this.setOderStatus,
        }}
      >
        {this.props.children}
      </InfosContext.Provider>
    );
  }
}
