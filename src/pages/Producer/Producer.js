import React, { useContext, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import "./Producer.css";
import "../Product/Product.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import ProductsList from "../../components/ProductsList/ProductsList";
import ProductCard from "../../components/ProductCard/ProductCard";
import SimpleButton from "../../components/SimpleButton/SimpleButton";
import Loading from "../../components/Loading/Loading";
import { InfosContext } from "../../context/Contex";
import { UserContext } from "../../context/userContext";
import Modal from "../../components/Modal/Modal";
import { Link } from "react-router-dom";

const Producer = () => {
  let { id } = useParams();
  let history = useHistory();
  // console.log("id: ", id);
  let { producers, loadingProducers, products, loadingProducts } = useContext(
    InfosContext
  );
  let { user } = useContext(UserContext);
  const [showAddBtn, setShowAddBtn] = useState(false);
  const [prodid, setProdID] = useState(null);

  if (loadingProducers || loadingProducts) {
    return <Loading />;
  }

  // console.log("lista producatori in producer page", producers);

  let producer = producers.find((producer) => {
    return producer.id === parseInt(id);
  });

  let currentUserProducts = products.filter((product) => {
    return product.producerId === producer.id;
  });

  // console.log("producatorul in producer page", producer);

  return (
    <Wrapper>
      <Title title={`${producer.firstName} ${producer.lastName}`} />
      <div className="general-infos-container">
        <div className="general-info-container">
          <p className="general-info-title">Email</p>
          <p className="general-info-description">{producer.email}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Numărul de telefon</p>
          <p className="general-info-description">{producer.phone}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Județ</p>
          <p className="general-info-description">{producer.city}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Localitate</p>
          <p className="general-info-description">{producer.village}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Stradă</p>
          <p className="general-info-description">{producer.street}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Bloc</p>
          <p className="general-info-description">{producer.bloc}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Scara</p>
          <p className="general-info-description">{producer.stair}</p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Numărul</p>
          <p className="general-info-description">{producer.no}</p>
        </div>
      </div>
      <div className="more-infos">
        <p className="more-infos-title">Informații suplimentare</p>
        <p className="more-infos-content">{producer.description}</p>
      </div>
      <Title title="Anunțurile producătorului" />
      <ProductsList>
        {currentUserProducts.length === 0 ? (
          <Title title="..." />
        ) : (
          currentUserProducts.map((product) => {
            return (
              <ProductCard
                key={product.id}
                id={product.id}
                producerId={product.producerId}
                producerFirstName={product.producerFirstName}
                producerLastName={product.producerLastName}
                vegetableId={product.vegetableId}
                vegetableName={product.vegetableName}
                availableQuantity={product.avaibleQuantity}
                unit={product.unit}
                image={product.image}
                homeDelivery={product.homeDelivery}
                details={product.details}
                valid={product.valid}
                alive={product.alive}
                pricePerUnit={product.pricePerUnit}
              >
                <SimpleButton
                  clickHandle={() => {
                    if (user.token === null) {
                      history.push("/login");
                    } else {
                      setShowAddBtn(true);
                      setProdID(product.id);
                    }
                  }}
                  content="Adaugă în coș"
                />
                <Link to={`/products/${product.id}`} className="link">
                  Detalii
                </Link>
              </ProductCard>
            );
          })
        )}
        {/* <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard>
        <ProductCard>
          <Button content="Adaugă în coș" />
          <Link to="#" className="link">
            Detalii
          </Link>
        </ProductCard> */}
      </ProductsList>
      {showAddBtn === false ? null : (
        <Modal
          id={prodid}
          clickCancel={() => {
            setShowAddBtn(false);
          }}
        ></Modal>
      )}
    </Wrapper>
  );
};

export default Producer;
