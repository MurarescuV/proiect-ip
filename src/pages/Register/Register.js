import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import "./Register.css";
import "../Login/Login.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import SimpleButton from "../../components/SimpleButton/SimpleButton";
import { InfosContext } from "../../context/Contex";
import Loading from "../../components/Loading/Loading";
import singUpClient from "../../userFunctions/singUpClient";
import singUpProducer from "../../userFunctions/singUpProducer";

const Register = () => {
  let { locations, loadingLocations } = useContext(InfosContext);
  let history = useHistory();
  if (loadingLocations) {
    <Loading />;
  }

  const [errorAlert, setErrorAlert] = useState(false);
  const [loading, setLoading] = useState(false);

  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [phone, setPhone] = useState("");
  const [village, setVillage] = useState("");
  const [street, setStreet] = useState("");
  const [bloc, setBloc] = useState("");
  const [stair, setStair] = useState("");
  const [no, setNo] = useState("");
  const [role, setRole] = useState("ROLE_CUMPARATOR");
  const [document, setDocument] = useState(null);
  const [description, setDescription] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrorAlert(false);
    setLoading(true);
    let client;
    let producer;
    if (role === "ROLE_CUMPARATOR") {
      client = {
        firstName,
        lastName,
        phone,
        email,
        password,
        city: "Suceava",
        country: "Romania",
        district: "Suceava",
        village,
        street,
        bloc,
        stair,
        no,
        validAccount: true,
        role,
      };
      let response = await singUpClient(client);
      setLoading(false);
      if (response) {
        // console.log("raspunsul de la inregistrare client este: ", response);
        history.push("/login");
        window.location.reload(true);
      } else {
        setErrorAlert(true);
      }
    } else {
      producer = {
        firstName,
        lastName,
        phone,
        email,
        password,
        city: "Suceava",
        district: "Suceava",
        country: "Romania",
        village,
        street,
        bloc,
        stair,
        no,
        document,
        description,
        active: false,
        validAccount: true,
        role,
      };
      let response = await singUpProducer(producer);
      setLoading(false);
      if (response) {
        history.push("/login");
        // console.log(
        //   "raspunsul de inregistrare de la producator este: ",
        //   response
        // );
        window.location.reload(true);
      } else {
        setErrorAlert(true);
      }
    }
  };

  return (
    <Wrapper>
      <Title title="Vă rugăm să vă înregistrați!" />
      <div className="register-form-group">
        <form className="register-form" onSubmit={handleSubmit}>
          <div className="input-group">
            <label htmlFor="lastName">Nume</label>
            <input
              type="text"
              id="lastName"
              name="lastName"
              onChange={(e) => setLastName(e.target.value)}
              value={lastName}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="firstName">Prenume</label>
            <input
              type="text"
              id="firstName"
              name="firstName"
              onChange={(e) => setFirstName(e.target.value)}
              value={firstName}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="username">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              onChange={(e) => setEmail(e.target.value)}
              value={email}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="username">Parolă</label>
            <input
              type="password"
              id="password"
              name="password"
              className="pass"
              onChange={(e) => setPassword(e.target.value)}
              value={password}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="phone">Număr de telefon</label>
            <input
              type="text"
              id="phone"
              name="phone"
              onChange={(e) => setPhone(e.target.value)}
              value={phone}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="village">Localitate</label>
            <select
              className="roleSelect"
              name="village"
              id="village"
              onChange={(e) => setVillage(e.target.value)}
              value={village}
              required
            >
              <option value="Selectează">Selectează</option>
              {locations.map((location) => {
                return (
                  <option key={location.id} value={location.diacritice}>
                    {location.diacritice}
                  </option>
                );
              })}

              {/* <option value="Darmanesti">Darmanesti</option>
              <option value="Suceava">Suceava</option> */}
            </select>
          </div>
          <div className="input-group">
            <label htmlFor="street">Strada</label>
            <input
              type="text"
              id="street"
              name="street"
              onChange={(e) => setStreet(e.target.value)}
              value={street}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="bloc">Bloc</label>
            <input
              type="text"
              id="bloc"
              name="bloc"
              onChange={(e) => setBloc(e.target.value)}
              value={bloc}
              required
              autoComplete="off"
            />
          </div>
          <div className="input-group">
            <label htmlFor="stair">Scara</label>
            <input
              type="text"
              id="stair"
              name="stair"
              onChange={(e) => setStair(e.target.value)}
              value={stair}
              required
              autoComplete="off"
            />
          </div>
          <div className="input-group">
            <label htmlFor="no">Numărul</label>
            <input
              type="number"
              id="no"
              name="no"
              onChange={(e) => setNo(e.target.value)}
              value={no}
              required
              autoComplete="off"
            />
          </div>
          <div className="input-group">
            <label htmlFor="role">Rol</label>
            <select
              className="roleSelect"
              name="role"
              id="role"
              onChange={(e) => setRole(e.target.value)}
              value={role}
            >
              <option value="ROLE_CUMPARATOR">Client</option>
              <option value="ROLE_PRODUCATOR">Producător</option>
            </select>
          </div>
          {role === "ROLE_PRODUCATOR" && (
            <div className="input-group">
              <label htmlFor="document">Autorizație</label>
              <label
                style={{
                  fontSize: "13px",
                  letterSpacing: "1px",
                  color: "white",
                }}
                className="link-button"
                htmlFor="document"
              >
                Incarcă fișier
                <input
                  type="file"
                  id="document"
                  name="document"
                  onChange={(e) => setDocument(e.target.files[0])}
                  required
                  autoComplete="off"
                  minLength="2"
                  style={{
                    width: "100%",
                    height: "100%",
                    display: "none",
                  }}
                />
              </label>
            </div>
          )}
          {role === "ROLE_PRODUCATOR" && (
            <div className="input-group">
              <label htmlFor="description">Descriere</label>
              <textarea
                className="register-textarea"
                name="description"
                id="description"
                cols="30"
                rows="10"
                onChange={(e) => setDescription(e.target.value)}
                value={description}
                required
                autoComplete="off"
                minLength="2"
              ></textarea>
            </div>
          )}
          <div
            className="input-group"
            style={{ justifyContent: "space-around" }}
          >
            {/* <input type="submit" value="Înregistrare" className="link-button" /> */}
            <SimpleButton content="Înregistrare" />
          </div>

          {loading && <p className="register-message">Se incarca...</p>}
          {errorAlert && (
            <p className="register-message" style={{ color: "#000" }}>
              Eroare la inregistrare...
            </p>
          )}
        </form>
      </div>
    </Wrapper>
  );
};

export default Register;
