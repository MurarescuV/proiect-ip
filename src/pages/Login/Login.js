import React, { useState, useContext } from "react";
import { useHistory } from "react-router-dom";
import "./Login.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import SimpleButton from "../../components/SimpleButton/SimpleButton";
import { UserContext } from "../../context/userContext";
import loginUser from "../../userFunctions/loginUser";
import changePassword from "../../userFunctions/changePassword";

const Login = () => {
  const history = useHistory();
  const { userLogin } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [errorAlert, setErrorAlert] = useState(false);
  const [loading, setLoading] = useState(false);

  const [changePass, setChangePass] = useState(false);
  const [tempEmail, setTempEmail] = useState("");

  const handleSubmit = async (e) => {
    e.preventDefault();
    setErrorAlert(false);
    setLoading(true);
    let response;
    response = await loginUser({ email, password });
    setLoading(false);
    if (response) {
      const { email, token, role } = response.data;
      const newUser = { email, token, role };
      userLogin(newUser);
      history.push("/");
      // console.log("raspunsul de la logare: ", response);
    } else {
      setErrorAlert(true);
    }
  };

  return (
    <Wrapper>
      <Title title="Vă rugăm să vă logați!" />
      <div className="login-form-group">
        <form className="login-form" onSubmit={handleSubmit}>
          <div className="input-group">
            <label htmlFor="email">Email</label>
            <input
              type="email"
              id="email"
              name="email"
              onChange={(e) => {
                setEmail(e.target.value);
              }}
              value={email}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <div className="input-group">
            <label htmlFor="password">Parolă</label>
            <input
              type="password"
              id="password"
              name="password"
              className="pass"
              onChange={(e) => {
                setPassword(e.target.value);
              }}
              value={password}
              required
              autoComplete="off"
              minLength="2"
            />
          </div>
          <input type="submit" className="link-button" value="Logare" />
          {/* <SimpleButton content="Logare" clickHandle={handleSubmit} /> */}
          <p className="login-message" style={{ margin: "10px" }}>
            <span
              onClick={() => {
                setChangePass(!changePass);
              }}
              className="reset-pass-message"
            >
              Schimbă parola
            </span>{" "}
            <br /> {"(Veți primi pe email noua parolă)"}
          </p>

          {changePass && (
            <>
              <div className="input-group">
                <label htmlFor="temp-email">Introduceți email-ul</label>
                <input
                  type="email"
                  id="temp-email"
                  name="tempEmail"
                  onChange={(e) => {
                    setTempEmail(e.target.value);
                  }}
                  value={tempEmail}
                  required
                  autoComplete="off"
                  minLength="2"
                />
              </div>
              <div className="input-group">
                <SimpleButton
                  content="Schimbă parola"
                  clickHandle={() => {
                    changePassword({ email: tempEmail });
                    setChangePass(!changePass);
                  }}
                />
              </div>
            </>
          )}
          {loading && <p className="login-message">Se incarcă...</p>}
          {errorAlert && (
            <p className="login-message" style={{ color: "#000" }}>
              Eroare la autentificare...
            </p>
          )}
        </form>
      </div>
    </Wrapper>
  );
};

export default Login;
