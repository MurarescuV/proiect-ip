import React, { useContext } from "react";
import "./Product.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import Loading from "../../components/Loading/Loading";
import { useParams } from "react-router-dom";
import { InfosContext } from "../../context/Contex";

const Product = () => {
  let { id } = useParams();
  let { products, loadingProducts } = useContext(InfosContext);

  if (loadingProducts) {
    return <Loading />;
  }

  let product = products.find((product) => {
    return product.id === parseInt(id);
  });

  // console.log("this product: ", product);
  return (
    <Wrapper>
      <Title title={product.vegetableName} />
      <div className="general-infos-container">
        <div className="general-info-container">
          <p className="general-info-title">Producător</p>
          <p className="general-info-description">
            {product.producerFirstName} {product.producerLastName}
          </p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Cantitate disponibilă</p>
          <p className="general-info-description">
            {product.avaibleQuantity} {product.unit}
          </p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Preț</p>
          <p className="general-info-description">
            {product.pricePerUnit} lei / {product.unit}
          </p>
        </div>
        <div className="general-info-container">
          <p className="general-info-title">Livrare la domiciliu</p>
          <p className="general-info-description">
            {product.homeDelivery ? "Da" : "Nu"}
          </p>
        </div>
        {/* <div className="general-info-container">
          <SimpleButton content="Adaugă în coș" />
        </div> */}
      </div>
      <div className="more-infos">
        <p className="more-infos-title">Informații suplimentare</p>
        <p className="more-infos-content">{product.details}</p>
      </div>
      <div className="photo-galery">
        <div className="photo-galery-item">
          <img src={product.image} alt="img" />
        </div>
        {/* <div className="photo-galery-item">
          <img src={product.image} alt="img" />
        </div>
        <div className="photo-galery-item">
          <img src={product.image} alt="img" />
        </div> */}
      </div>
    </Wrapper>
  );
};

export default Product;
