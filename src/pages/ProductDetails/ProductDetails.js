import React, { useContext, useState } from "react";
import { Link, useHistory } from "react-router-dom";
import "./ProductDetails.css";
import "../Register/Register.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import { InfosContext } from "../../context/Contex";
import { UserContext } from "../../context/userContext";
import Loading from "../../components/Loading/Loading";
import Button from "../../components/Button/Button";
import addProduct from "../../userFunctions/addProduct";

const ProductDetails = (props) => {
  let [vegetableId, setVegetableId] = useState("Selectează");
  let [avaibleQuantity, setAvaibleQuantity] = useState("");
  let [unit, setUnit] = useState("");
  let [image, setImage] = useState("");
  let [homeDelivery, setHomeDelivery] = useState(true);
  let [details, setDetails] = useState("");
  let [pricePerUnit, setPricePerUnit] = useState("");

  let history = useHistory();
  let { user } = useContext(UserContext);
  let { loadingAliments, loadingProducers, aliments, producers } = useContext(
    InfosContext
  );
  if (
    loadingAliments &&
    loadingProducers &&
    (user.email === undefined || user.email === null)
  ) {
    return <Loading />;
  }

  let currentProducer = producers.find((producer) => {
    return producer.email === user.email;
  });

  return (
    <Wrapper>
      <form className="product-details-form-group">
        <div className="product-details-input-group">
          <label htmlFor="product-name">Denumire produs</label>
          <select
            className="roleSelect"
            name="product-name"
            id="product-name"
            onChange={(e) => setVegetableId(e.target.value)}
            value={vegetableId}
          >
            <option value="Selectează">Selectează</option>
            {aliments.map((aliment) => {
              return (
                <option key={aliment.id} value={aliment.id}>
                  {aliment.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="available-quantity">Cantitate disponibilă</label>
          <input
            type="number"
            id="available-quantity"
            name="avaibleQuanitty"
            onChange={(e) => setAvaibleQuantity(e.target.value)}
            value={avaibleQuantity}
            required
            autoComplete="off"
          />
        </div>
        <div className="product-details-input-group">
          <label htmlFor="unit">Unitate de măsură</label>
          <input
            type="text"
            id="unit"
            name="unit"
            onChange={(e) => setUnit(e.target.value)}
            value={unit}
            required
            autoComplete="off"
          />
        </div>
        <div className="product-details-input-group">
          <label htmlFor="image">Imagine</label>
          <label
            style={{ fontSize: "13px", letterSpacing: "1px", color: "white" }}
            className="link-button"
            htmlFor="image"
          >
            Incarcă fișier
            <input
              type="file"
              id="image"
              name="image"
              onChange={(e) => setImage(e.target.files[0])}
              required
              autoComplete="off"
              minLength="2"
              style={{
                width: "100%",
                height: "100%",
                display: "none",
              }}
            />
          </label>
        </div>
        <div className="product-details-input-group">
          <label htmlFor="home-delivery">Livrare la domiciliu</label>
          <select
            className="roleSelect"
            name="homeDelivery"
            id="home-delivery"
            onChange={(e) => setHomeDelivery(e.target.value)}
            value={homeDelivery}
          >
            <option value={true}>Da</option>
            <option value={false}>Nu</option>
          </select>
        </div>

        <div className="product-details-input-group">
          <label htmlFor="price-per-unit">Preț pe unitate</label>
          <input
            type="number"
            id="price-per-unit"
            name="pricePerUnit"
            onChange={(e) => setPricePerUnit(e.target.value)}
            value={pricePerUnit}
            required
            autoComplete="off"
          />
        </div>
        <div className="product-details-input-group">
          <label htmlFor="details">Informații suplimentare</label>
          {/* <input
            type="text"
            id="details"
            name="details"
            onChange={(e) => setDetails(e.target.value)}
            value={details}
          /> */}
          <textarea
            className="register-textarea"
            name="details"
            id="details"
            cols="30"
            rows="5"
            onChange={(e) => setDetails(e.target.value)}
            value={details}
            required
            autoComplete="off"
            minLength="2"
          ></textarea>
        </div>
      </form>
      <div className="product-details-form-group">
        <div className="product-details-input-group">
          <Button
            content="Adaugă anunț"
            clickHandle={() => {
              addProduct({
                producerId: currentProducer.id,
                vegetableId,
                avaibleQuantity,
                unit,
                image,
                homeDelivery,
                details,
                valid: true,
                alive: true,
                pricePerUnit,
              });
              history.push("/my-products");
            }}
          />
        </div>
        <div className="product-details-input-group">
          <Link to="/my-products" className="link-button">
            Înapoi
          </Link>
        </div>
      </div>
    </Wrapper>
  );
};

export default ProductDetails;
