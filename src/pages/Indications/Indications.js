import React, { Component } from "react";
import Banner from "../../components/Banner/Banner";
import InfosList from "../../components/InfosList/InfosList";
import Title from "../../components/Title/Title";
import Wrapper from "../../components/Wrapper/Wrapper";
import "./Indications.css";
class Indications extends Component {
  render() {
    return (
      <Wrapper>
        <Banner
          src="https://images.pexels.com/photos/1838552/pexels-photo-1838552.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
          content="Experiențele frumoase te leagă de un loc"
        />
        <Title title="Indicații la cumpărături" />
        <div className="indicatii-container">
          <div className="indicatii-item">
            <h1>Sfat</h1>
            <p>
              Prin simpli pași, chiar și aici la “tarabă” poți avea parte de
              experiențe memorabile pentru care te vei întoarce pe platforma
              noastră.
            </p>
          </div>

          <div className="indicatii-item">
            <h1>Pași</h1>
            <ul>
              <li>- Urmăreste-ți nevoile</li>
              <li>- Filtrează anunțurile</li>
              <li>- Pune în coș produsele</li>
              <li>- Plasează comanda</li>
              <li>- Verifică confirmarea</li>
              <li>- În scurt timp vei avea și produsele</li>
            </ul>
          </div>

          <div className="indicatii-item">
            <h1>Sfaturi pentru fiecare</h1>
            <p>
              Către client: pentru o experiență mai frumoasă alege să ai un cont
              aici și totul va fi mai simplu. <br />
              Către producător: să ajungi printre producătorii de încredere ai
              fiecărui client oferă calitate la fiecare produs! Nu uita,
              păstrează-ți anunțurile permanent la curent cu stocurile din
              hambar!
            </p>
          </div>

          <div className="indicatii-item">
            <img
              className="indicatii-image"
              src="https://images.pexels.com/photos/6316534/pexels-photo-6316534.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
              alt="img"
            />
          </div>
        </div>
        <Title title="Nu uita de diversitate" />
        <InfosList />
        <div className="indicatii-container">
          <div className="indicatii-item">
            <h1>Direcția pentru Agricultură Județeană Suceava</h1>
            <p>
              este responsabilă cu implemetarea in județul Suceava, a
              politicilor și strategiilor Ministerului Agriculturii și
              Dezvoltării Rurale în domeniile de activitate ale acestuia,
              statistică și RICA, asistență tehnică, informarea, îndrumarea și
              consilierea pentru obținerea fondurilor europene și naționale în
              rândul fermierilor, efectuarea activităților de monitorizare,
              verificare, inspecții și control în domeniile de activitate,
              precum și cu alte atribuții prevăzute de legislația în vigoare.
            </p>
          </div>

          <div className="indicatii-item">
            <img
              className="indicatii-image"
              src="https://images.pexels.com/photos/296230/pexels-photo-296230.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
              alt="img"
            />
          </div>
        </div>
      </Wrapper>
    );
  }
}

export default Indications;
