import React from "react";
import "./Home.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import InfoList from "../../components/InfosList/InfosList";
import PhotoContainer from "../../components/PhotoContainer/PhotoContainer";
import HomeDetails from "../../components/HomeDetails/HomeDetails";
import BannerHome from "../../components/BannerHome/BannerHome";

const Home = () => {
  return (
    <Wrapper>
      {/* componenta de sus */}
      <BannerHome />
      <Title title="Diversitate" />
      <InfoList />
      <PhotoContainer src="https://images.pexels.com/photos/3658482/pexels-photo-3658482.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500" />
      <Title title="O scurtă descriere a platformei" />
      <HomeDetails
        firstContent=" LaTarani.ro este platforma online ce pune împreună familiile de la oraș cu gospodăriile de la țară. Aici descoperi micii producători de lângă tine sau pe acei harnici gospodari care produc alimente sau artizanat autentic."
        secondContent="Poți comanda de la ei, sau poți găsi informații despre cum poți ajunge la ei, pentru a-i cunoaște. Cumpără local, cumpără românește! Cumpărând de la acești producători hrănești două familii: a ta și a lor!"
      />
    </Wrapper>
  );
};

export default Home;
