import React, { useContext, useState } from "react";
import ShopCartProduct from "../../components/ShopCartProduct/ShopCartProduct";
import Title from "../../components/Title/Title";
import Wrapper from "../../components/Wrapper/Wrapper";
import "./ShopCart.css";
import { InfosContext } from "../../context/Contex";
import submitOrder from "../../userFunctions/submitOrder";

const ShopCart = () => {
  let {
    cartProductsList,
    removeProductFromCart,
    clearLocalStorage,
  } = useContext(InfosContext);
  let prices;
  if (cartProductsList.length === 0) {
    prices = [0];
  } else {
    prices = cartProductsList.map(
      (product) => product.pricePerUnit * product.quantity
    );
  }
  let [isSumbit, setIsSubmit] = useState("");
  // console.log("preturi: ", prices);

  let reducer = (acumulator, currentValue) => acumulator + currentValue;
  let orderValue = prices.reduce(reducer);

  const submitOrderHandle = async () => {
    let response = await submitOrder();
    if (response) {
      setIsSubmit(response.data);
    }
    clearLocalStorage();
    setTimeout(() => {
      setIsSubmit("");
      window.location.reload(true);
    }, 2000);
  };

  return (
    <Wrapper>
      <Title title="Coș cumpărături" />

      <div className="shop-cart-container">
        <div className="shop-cart-item">
          {cartProductsList.map((product, index) => {
            return (
              <ShopCartProduct
                id={product.id}
                key={index}
                img={product.image}
                name={product.vegetableName}
                price={product.pricePerUnit}
                quantity={product.quantity}
                producer={`${product.producerFirstName} ${product.producerLastName}`}
                unit={product.unit}
                clickHandle={() => removeProductFromCart(product)}
              />
            );
          })}
          {/* <ShopCartProduct
            name="bracoliii"
            price={199}
            cantity={30}
            producer="Muraresc2"
          />
          <ShopCartProduct
            name="bracoliii"
            price={199}
            cantity={30}
            producer="Muraresc2"
          />
          <ShopCartProduct
            name="bracoliii"
            price={199}
            cantity={30}
            producer="Muraresc2"
          /> */}
        </div>

        <div className="shop-cart-item">
          <div className="shop-cart-total">
            <div className="cart-price">
              <p>Valoare comandă:</p>
              <p>{orderValue} lei</p>
            </div>

            <hr></hr>

            <button
              className="shop-cart-button"
              onClick={
                cartProductsList.length !== 0 ? submitOrderHandle : undefined
              }
            >
              Finalizează comanda
            </button>
            {isSumbit && (
              <p className="submit-order-message">
                Comandă a fost plasată cu succes!
              </p>
            )}
          </div>
        </div>
      </div>
    </Wrapper>
  );
};

export default ShopCart;

// class ShopCart extends Component {
//   render() {
//     return (
//       <Wrapper>
//         <Title title="Cos cumparaturi" />

//         <div className="shop-cart-container">
//           <div className="shop-cart-item">
//             <ShopCartProduct
//               name="bracoliii"
//               price={199}
//               cantity={30}
//               producer="Muraresc2"
//             />
//             <ShopCartProduct
//               name="bracoliii"
//               price={199}
//               cantity={30}
//               producer="Muraresc2"
//             />
//             <ShopCartProduct
//               name="bracoliii"
//               price={199}
//               cantity={30}
//               producer="Muraresc2"
//             />
//           </div>

//           <div className="shop-cart-item">
//             <div className="shop-cart-total">
//               <div className="cart-price">
//                 <p>valoare comanda:</p>
//                 <p>129 lei</p>
//               </div>

//               <hr></hr>

//               <div className="cart-price">
//                 <p>Total:</p>
//                 <p>129 lei</p>
//               </div>

//               <hr></hr>

//               <button className="shop-cart-button">Finalizeaza comanda</button>
//             </div>
//           </div>
//         </div>
//       </Wrapper>
//     );
//   }
// }

// export default ShopCart;
