import React, { useContext, useState } from "react";
import { useHistory } from "react-router-dom";
import "./Products.css";
import ProductCard from "../../components/ProductCard/ProductCard";
import SimpleButton from "../../components/SimpleButton/SimpleButton";
import { Link } from "react-router-dom";
import Wrapper from "../../components/Wrapper/Wrapper";
import Title from "../../components/Title/Title";
import ProductsList from "../../components/ProductsList/ProductsList";
import Filters from "../../components/Filters/Filters";
import ProducersList from "../../components/ProducersList/ProducersList";
import Banner from "../../components/Banner/Banner";
import Loading from "../../components/Loading/Loading";
import { InfosContext } from "../../context/Contex";
import { UserContext } from "../../context/userContext";
import Modal from "../../components/Modal/Modal";

const Products = () => {
  let history = useHistory();
  let {
    loadingProducts,
    filteredProducts,
    loadingFilteredProducts,
  } = useContext(InfosContext);
  let { user } = useContext(UserContext);
  const [showAddBtn, setShowAddBtn] = useState(false);
  const [id, setID] = useState(null);
  // console.log("products in products page:", products);
  if (loadingProducts || loadingFilteredProducts) {
    return <Loading />;
  }

  return (
    <Wrapper>
      {/* componenta de sus */}
      <Banner
        src="https://images.pexels.com/photos/4994698/pexels-photo-4994698.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"
        content="Incarcă-ți coșul cu produse de calitate pentru casa ta!"
      />
      <Title title="listă anunțuri" />
      <Filters />
      {filteredProducts.length === 0 ? (
        <Title title="..." />
      ) : (
        <ProductsList>
          {filteredProducts.map((product) => {
            return (
              <ProductCard
                key={product.id}
                id={product.id}
                producerId={product.producerId}
                producerFirstName={product.producerFirstName}
                producerLastName={product.producerLastName}
                vegetableId={product.vegetableId}
                vegetableName={product.vegetableName}
                availableQuantity={product.avaibleQuantity}
                unit={product.unit}
                image={product.image}
                homeDelivery={product.homeDelivery}
                details={product.details}
                valid={product.valid}
                alive={product.alive}
                pricePerUnit={product.pricePerUnit}
              >
                {/* <SimpleButton
                content="Adaugă în coș"
                clickHandle={() => setCartProducts(product)}
              /> */}

                <SimpleButton
                  clickHandle={() => {
                    if (user.token === null) {
                      history.push("/login");
                    } else {
                      setShowAddBtn(true);
                      setID(product.id);
                    }
                  }}
                  content="Adaugă în coș"
                />
                <Link
                  to={`/products/${product.id}`}
                  className="link-second-button"
                >
                  Detalii
                </Link>
              </ProductCard>
            );
          })}
        </ProductsList>
      )}

      <Title title="Listă producători" />
      <ProducersList />
      {showAddBtn === false ? null : (
        <Modal
          id={id}
          clickCancel={() => {
            setShowAddBtn(false);
          }}
        ></Modal>
      )}
    </Wrapper>
  );
};

export default Products;
