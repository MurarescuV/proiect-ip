import React, { useContext } from "react";
import "./MyProducts.css";
import Wrapper from "../../components/Wrapper/Wrapper";
import Button from "../../components/Button/Button";
import ProductsList from "../../components/ProductsList/ProductsList";
import ProductCard from "../../components/ProductCard/ProductCard";
import Title from "../../components/Title/Title";
import Banner from "../../components/Banner/Banner";
import { UserContext } from "../../context/userContext";
import { InfosContext } from "../../context/Contex";
import Loading from "../../components/Loading/Loading";
import editProduct from "../../userFunctions/editProduct";
import { Link } from "react-router-dom";
import SimpleButton from "../../components/SimpleButton/SimpleButton";
import EditProduct from "../../components/EditProduct/EditProduct";
import EditUser from "../../components/EditUser/EditUser";

const MyProducts = () => {
  let { user } = useContext(UserContext);
  let {
    loadingProducts,
    loadingProducers,
    loadingClients,
    products,
    producers,
    clients,
    setEditProductInfos,
    showEditProduct,
    showEditUser,
    setEditProducerInfos,
    setEditClientInfos,
  } = useContext(InfosContext);
  let currentUser;
  let currentUserProducts;
  // console.log("user ul actual este: ", user);

  if (
    loadingProducts ||
    loadingProducers ||
    loadingClients ||
    user.token === null
  ) {
    return <Loading />;
  }
  // if (user.token === null) {
  //   return <Loading />;
  // }
  // console.log("producatorii de acum sunt:", producers);
  // console.log("prudusele de acum sunt:", products);

  if (user.role === "ROLE_PRODUCATOR") {
    currentUser = producers.find((producer) => {
      return producer.email === user.email;
    });
  } else {
    currentUser = clients.find((client) => {
      return client.email === user.email;
    });
  }

  // console.log("user de acum este:", currentUser);
  // console.log(currentUser);
  if (currentUser === null || currentUser === undefined) {
    return <Loading />;
  }

  // console.log("current user ", currentUser);

  currentUserProducts = products.filter((product) => {
    return product.producerId === currentUser.id;
  });

  // console.log("anunturile userului actual: ", currentUserProducts);

  // console.log("anuntul de editat este: ", editProductInfos);
  // console.log("valoarea lui showEdit este: ", showEditProduct);

  return (
    <Wrapper>
      {/* componenta de sus */}
      <Banner
        src="https://images.pexels.com/photos/1287135/pexels-photo-1287135.jpeg?auto=compress&cs=tinysrgb&h=750&w=1260"
        content="Indicațiile corecte date unei persoane o pot duce repede la tine. Fii tu ținta!"
      />
      <div className="manage-products-container">
        {/* <SimpleButton content="Adaugă descriere" /> */}
        {currentUser.role === "ROLE_PRODUCATOR" && currentUser.active === true && (
          <Link className="link-button" to="/product-details-add">
            Adaugă anunț
          </Link>
        )}

        <Button
          content="Modifică datele personale"
          clickHandle={() => {
            if (currentUser.role === "ROLE_PRODUCATOR") {
              setEditProducerInfos({
                id: currentUser.id,
                clientId: currentUser.clientId,
                firstName: currentUser.firstName,
                lastName: currentUser.lastName,
                phone: currentUser.phone,
                email: currentUser.email,
                password: "",
                locationId: currentUser.locationId,
                city: currentUser.city,
                district: currentUser.district,
                country: currentUser.country,
                village: currentUser.village,
                street: currentUser.street,
                bloc: currentUser.bloc,
                stair: currentUser.stair,
                no: currentUser.no,
                document: currentUser.document,
                description: currentUser.description,
                active: currentUser.active,
                validAccount: currentUser.validAccount,
                role: currentUser.role,
              });
            } else {
              setEditClientInfos({
                id: currentUser.id,
                firstName: currentUser.firstName,
                lastName: currentUser.lastName,
                phone: currentUser.phone,
                email: currentUser.email,
                password: "",
                locationId: currentUser.locationId,
                city: currentUser.city,
                district: currentUser.district,
                country: currentUser.country,
                village: currentUser.village,
                street: currentUser.street,
                bloc: currentUser.bloc,
                stair: currentUser.stair,
                no: currentUser.no,
                validAccount: currentUser.validAccount,
                role: currentUser.role,
              });
            }
          }}
        />
      </div>
      {showEditUser && <EditUser role={currentUser.role} />}

      {currentUser.role === "ROLE_PRODUCATOR" && (
        <>
          {" "}
          <Title title="Anunțurile mele" />
          <ProductsList>
            {currentUserProducts.length === 0 ||
            currentUser.role === "ROLE_CUMPARATOR" ? (
              <Title title="..." />
            ) : (
              currentUserProducts.map((product) => {
                return (
                  <ProductCard
                    key={product.id}
                    id={product.id}
                    producerId={product.producerId}
                    producerFirstName={product.producerFirstName}
                    producerLastName={product.producerLastName}
                    vegetableId={product.vegetableId}
                    vegetableName={product.vegetableName}
                    availableQuantity={product.avaibleQuantity}
                    unit={product.unit}
                    image={product.image}
                    homeDelivery={product.homeDelivery}
                    details={product.details}
                    valid={product.valid}
                    alive={product.alive}
                    pricePerUnit={product.pricePerUnit}
                  >
                    <Button
                      content="Editează"
                      clickHandle={() => {
                        setEditProductInfos({
                          id: product.id,
                          producerId: product.producerId,
                          vegetableId: product.vegetableId,
                          avaibleQuantity: product.avaibleQuantity,
                          unit: product.unit,
                          image: product.image,
                          homeDelivery: product.homeDelivery,
                          details: product.details,
                          valid: product.valid,
                          alive: product.alive,
                          pricePerUnit: product.pricePerUnit,
                        });
                      }}
                    />
                    <SimpleButton
                      content="Șterge"
                      clickHandle={() =>
                        editProduct({
                          id: product.id,
                          producerId: product.producerId,
                          vegetableId: product.vegetableId,
                          avaibleQuantity: product.avaibleQuantity,
                          unit: product.unit,
                          image: product.image,
                          homeDelivery: product.homeDelivery,
                          details: product.details,
                          valid: product.valid,
                          alive: false,
                          pricePerUnit: product.pricePerUnit,
                        })
                      }
                    />
                  </ProductCard>
                );
              })
            )}

            {/* <ProductCard>
          <SimpleButton content="Editează" />
          <Button content="Șterge" />
        </ProductCard>
        <ProductCard>
          <SimpleButton content="Editează" />
          <Button content="Șterge" />
        </ProductCard>
        <ProductCard>
          <SimpleButton content="Editează" />
          <Button content="Șterge" />
        </ProductCard>
        <ProductCard>
          <SimpleButton content="Editează" />
          <Button content="Șterge" />
        </ProductCard> */}
          </ProductsList>
        </>
      )}

      {showEditProduct && <EditProduct />}
    </Wrapper>
  );
};

export default MyProducts;
